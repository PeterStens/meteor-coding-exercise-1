/*****************************************************************************/
/* Home: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Home.events({
    
  'submit #playerForm': function (event, tmpl) {
   
   event.preventDefault();
   
   var player = {};
   
   player.firstName = event.target.firstName.value;
   player.lastName = event.target.lastName.value;
   player.email = event.target.email.value;
   player.eyeColor = event.target.eyeColor.value;
   player.favColor = event.target.favColor.value;
   
    Meteor.call('/app/process', player, function(err, response) {
			console.log(JSON.stringify(response));
            Session.set('result',JSON.stringify(response));
            
            if (err) {
                
                alert(err);
            }
		});
  }
  
});

Template.Home.helpers({
 result: function () {
       return Session.get('result');
     }
});

/*****************************************************************************/
/* Home: Lifecycle Hooks */
/*****************************************************************************/
Template.Home.created = function () {
};

Template.Home.rendered = function () {
};

Template.Home.destroyed = function () {
};